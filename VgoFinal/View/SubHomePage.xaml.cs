﻿using System;
using System.Collections.Generic;
using System.Linq;
using VgoFinal.Model;
using VgoFinal.ViewModel;
using Xamarin.Forms;

namespace VgoFinal.View
{
    public partial class SubHomePage : ContentPage
    {
        public SubHomePage()
        {
            InitializeComponent();
            BindingContext = new MyListPageViewModel();
        }

        private async void OnItemSelected(Object sender, ItemTappedEventArgs e)
        {
            var mydetails = e.Item as MyListModel;
            await Navigation.PushAsync(new MyListPageDetail(mydetails.Name, mydetails.Ingredients, mydetails.Image, mydetails.PhpPrice));

        }

        void SearchBar_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            var _containter = BindingContext as MyListPageViewModel;
            FL.BeginRefresh();

            if (string.IsNullOrWhiteSpace(e.NewTextValue))
                FL.ItemsSource = _containter.FoodList;
            else
                FL.ItemsSource = _containter.FoodList.Where(i => i.Name.Contains(e.NewTextValue));

            FL.EndRefresh();
        }
    }
}
