﻿using System;
using System.Collections.Generic;
using System.IO;
using SQLite;
using VgoFinal.Tables;
using Xamarin.Forms;

namespace VgoFinal.View
{
    public partial class RegistrationPage : ContentPage
    {
        public RegistrationPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            var dbpath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "UserDatabase.db");
            var db = new SQLiteConnection(dbpath);
            db.CreateTable<RegUserTable>();

            var item = new RegUserTable()
            {
                UserName = EntryUserName.Text,
                Password = EntryUserPassword.Text,
                Email = EntryUserEmail.Text,
                PhoneNumber = EntryUserPhoneNumber.Text
            };
            db.Insert(item);
            Device.BeginInvokeOnMainThread(async () =>
            {



                var result = await this.DisplayAlert("Congratulations", "User registration successful", "yes", "cancel");

                if (result)
                    await Navigation.PushAsync(new LoginPage());

            });
        }
    }
}
