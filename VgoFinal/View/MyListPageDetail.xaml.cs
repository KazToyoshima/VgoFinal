﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace VgoFinal.View
{
    public partial class MyListPageDetail : ContentPage
    {
        public MyListPageDetail(string name, string ingredients, string image, string price)
        {
            InitializeComponent();
            MyPriceShow.Text = price;
            MyItemNameShow.Text = name;
            MyIngrediantItemShow.Text = ingredients;
            MyImageCall.Source = new UriImageSource()
            {
                Uri = new Uri(image)
            };
        }
    }
}
