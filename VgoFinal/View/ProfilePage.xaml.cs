﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using SkiaSharp;
using Microcharts;
using Entry = Microcharts.Entry;

namespace VgoFinal.View
{
    public partial class ProfilePage : ContentPage
    {



        public ProfilePage()
        {
            InitializeComponent();
            UsrN.Text = GlobalVariableClass.VariableOne;


        }

        void TapGestureRecognizer_Tapped(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new ChartPage());
        }

        void TapGestureRecognizer_Tapped_1(System.Object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new TestTabPage());
        }
    }
}
