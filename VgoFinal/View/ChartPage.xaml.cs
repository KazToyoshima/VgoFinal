﻿using System;
using System.Collections.Generic;
using Microcharts;
using Entry = Microcharts.Entry;
using Xamarin.Forms;
using SkiaSharp;

namespace VgoFinal.View
{
    public partial class ChartPage : ContentPage
    {
        List<Entry> entw = new List<Entry>
        {
            new Entry(200)
            {
                Color = SKColor.Parse("#FF1493"),
                ValueLabel = "200"
            },
             new Entry(-200)
            {
                Color = SKColor.Parse("#FF1493"),
                ValueLabel = "200"
            },
              new Entry(10)
            {
                Color = SKColor.Parse("#FF1493"),
                ValueLabel = "200"
            }
        };
        public ChartPage()
        {
            InitializeComponent();
            chart1.Chart = new LineChart { Entries = entw };
        }
    }
}
